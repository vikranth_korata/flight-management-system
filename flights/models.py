# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.core.validators import RegexValidator
from django.core.validators import MaxValueValidator,MaxLengthValidator
from django.core.validators import MinValueValidator, MinLengthValidator
#from django.contrib.postgres.fields import JSONField



# Create your models here.
class Flight(models.Model):
    airline = models.CharField(max_length=75 )
    sector_from = models.CharField(max_length=75)
    sector_to = models.CharField(max_length=75)
    number_of_seats = models.IntegerField(default = 1)

    def __str__(self):
        return self.airline


class City(models.Model):
    city = models.CharField(max_length = 75)
    airport_code = models.CharField(max_length = 3 , validators=[RegexValidator(regex='^[A-Z]{3}$', message='Length has to be 3 and capital letters', code='nomatch')])

    def __str__(self):
        return self.airport_code

class User(models.Model):
    name = models.CharField(max_length = 75)
    age = models.IntegerField(validators=[MaxValueValidator(100),MinValueValidator(1)])
    passport_number = models.CharField(max_length = 8, validators=[RegexValidator(regex='^.{8}$', message='Length has to be 8', code='nomatch')], null = False)
    email = models.EmailField(max_length=254, unique=True,db_index=True, primary_key=True)

    def __str__(self):
        return self.name

class Bookings(models.Model):


    airline = models.CharField(max_length = 75)
    user_details = models.CharField(max_length = 75)


    def __str__(self):
        return self.user_details

class Org(models.Model):
    org_id = models.CharField(primary_key = True, max_length = 50)
    address = models.TextField(default = 'Here')
    gstin_no = models.CharField(max_length = 15,default = '11XXXXX1111XXXX', validators = [RegexValidator(regex = '^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$', message = 'Enter valid gstin number')])
    CURRENCY = [
    ('INR', 'Indian Rupee'),
    ('USD', 'US Dollar'),
    ('EUR','Euro'),
    ('CNY','Chinese Yuan')
    ]
    basic_currency = models.CharField(max_length = 3, choices = CURRENCY, default = 'INR')
    def __str__(self):
        return self.org_id

    def as_dict(self):
        return {
            "org_id":self.org_id,
            "address":self.address,
            "gstin_no":self.gstin_no,
            "basic_currency":self.basic_currency
        }

class UserDetails(models.Model):
    id = models.CharField(primary_key = True, max_length = 50)
    email = models.EmailField(max_length=254, unique = True)
    phone_number = models.BigIntegerField(default = 1111111111, validators = [RegexValidator(regex = '^[0-9]{10}$', message='Enter valid phone number')])
    org_details = models.ForeignKey(Org, on_delete = models.CASCADE)
    #extra_details = JSONField()
    def __str__(self):
        return self.id
    def as_dictt(self):
        return{
            "id":self.id,
            "email":self.email,
            "phone_number":self.phone_number,
            "org_details":self.org_details,
            #"extra_details":self.extra_details
        }

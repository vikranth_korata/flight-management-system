# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from django.views.generic import View
from models import Flight
from models import Bookings
from models import User
from models import City
from models import Org, UserDetails
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.core import serializers
import json
from django.http import QueryDict
from django.core.exceptions import *
from django.db import *


class flights(View):

    def get(self,request,fromcity, tocity):
        #fromcity is the the city from which user starts journey
        #tocity is the destination city of the user
        #airline gives the available airlines between those two cities
        #If there are no airlines or if no input given, reflects the same
        plf = fromcity
        plt = tocity
        if not plf or not plt:
            return HttpResponse("Please enter both start and destination cities!")
        else:
            _params = request.GET

            fromto = Flight.objects.filter(pk=2)
            #data = {}
            #data[""]
            #out1 = ', '.join([p.airline for p in fromto])
            out1 = list(fromto)
            data = serializers.serialize('json',fromto)

            if not out1:
                return HttpResponse("Oops! There are no Flights from '" + plf + "' to '" + plt + "'")
            else:
                #return HttpResponse("The airlines available between " + plf + " and " + plt + " are " + out1)
                return JsonResponse("The airlines available are " + data, safe =False)


class bookings(View):

    def get(self,request,username):
        # username is the name of the user which is already present in databases
        # airline here gives the airlines prviously booked by the user
        #output gives the details of previous travels
        # Username shouldnt be Empty. If the user didnt book any flights previously or not user it reflects the same.
        outr = username
        if not outr:
            return HttpResponse("Username cannot be empty!")
        else:
            _params = request.GET
            username = Bookings.objects.filter(user_details = username)
            out3 = ', '.join(k.airline for k in username)
            if not out3:
                return HttpResponse("We are sorry '" + outr + "'!! It seems like you haven't booked any flight previously or you are not a registered user.")
            else:
                return (HttpResponse("Hii " +outr + "!! Previously you have booked tickets to:  " + out3))


class flightsbyairline(View):

    def get(self,request,airline):
        # airline is the airline user desires to travel
        # sector_from and sector_to give the available from and to cities for the airline
        # Gives from and to cities between which airline travels
        #airline shouldnt be empty, it should be in data base. If not reflects the same.
        outy = airline
        if not outy:
            return HttpResponse("Please enter airline name!")
        else:
            _params = request.GET
            ouut = airline
            airline = Flight.objects.filter(airline = airline)
            out4 = ', '.join(x.sector_from for x in airline)
            out5 = ', '.join(y.sector_to for y in airline)
            if not out4 or not out5:
                return HttpResponse("Sorry! airline '" + ouut + "' doesn't exist!")
            else:
                return HttpResponse("The airline " + outy +  " travels from cites " + out4 +" to cities "+ out5 + " respectively.")


class booking(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, airline, username):
        return super(booking, self).dispatch(request, airline, username)
    def post(self, request, airline, username):
        if not self or not request or not airline or not username:
            return HttpResponse("Both username and airline should be filled")
        else:
            usernam = User.objects.filter(name = username)
            existing = ' '.join(s.name for s in usernam)
            if not existing:
                return HttpResponse("You are not a registered user. Please, register first to book a ticket.")
            else:
                airlin = Flight.objects.filter(airline = airline)
                exis = ' '.join(a.airline for a in airlin)
                if not exis:
                    return HttpResponse("The selcted airline doesn't exist.")
                else:
                    #airline and username are entered by the user
                    # With those parameters, a flight is booked and the same is entered in the database
                    # Both airline and username are to be filled.
                    # checks if user is registered previously and if not reflects the same.
                    # checks if airline exists and if not reflects the same.
                    #if everything is good books a ticket for the user in required airways
                    _params = request.GET
                    out = Bookings(airline = airline, user_details = username )
                    out.save()
                    return HttpResponse("Ticket booked Successfully!!")


class citycode(View):

    def get(self, request, cityname):
        _params = request.GET
        out55 = cityname
        if not out55:
            # City name shouldnt be empty
            return HttpResponse("Please enter the city name!")
        else:
            cityname = City.objects.filter(city = cityname)
            out77 = '\n'. join(s.airport_code for s in cityname)
            if not out77:
                #checks if the city details are present in the database
                return HttpResponse("Sorry! Airport code for " + out55 + " is not found.")
            else:
                # Gives the required airport code.
                return HttpResponse("The Airport code for " + out55 + " is " + out77)

class register(View):
    @method_decorator(csrf_exempt)
    def dispatch(self,request,name, age, passport, email ):
        return super(register, self).dispatch(request, name, age, passport, email)
    def post(self, request, name, age, passport, email):
        if not name or not age or not passport or not email:
            # No field should be empty
            return HttpResponse("Please provide all the details")
        else:
            _params = request.GET
            email1 = User.objects.filter(email = email)
            exist = ' '.join(j.email for j in email1)
            if not exist:
                passport1 = User.objects.filter(passport_number = passport)
                exists = ' '.join(l.passport_number for l in passport1)
                if not exists:
                    print len(passport)
                    reg = User(name = name, age = age, passport_number = passport, email=email )
                    reg.save()
                    return HttpResponse("Welcome " + name + "!! You are now a registered user.")
                    # Registers the user details in the database.
                else:
                    return HttpResponse("The passport number already exists. Try another.")
            else:
                return HttpResponse("user with this email already taken.. please provide unique email")

class org(View):

    def post(self,request):
        #import pdb;pdb.set_trace()
        _params = request.POST
        org_id = _params.get('org_id')
        address = _params.get('address')
        gstin_no = _params.get('gstin_no')
        basic_currency = _params.get('basic_currency')
        if not org_id or not address or not gstin_no or not basic_currency:
            return HttpResponse("Enter all the fields")
        else:
            c = Org.objects.filter(gstin_no = gstin_no)
            cd = '\n'.join(d.gstin_no for d in c)
            if not cd:
                try:
                    Org.objects.create(org_id = org_id, address = address, gstin_no = gstin_no, basic_currency = basic_currency)
                    return HttpResponse("Organisation instance created successfully!")
                except IntegrityError:
                    return HttpResponse("Organisation with this id already exists")
                except Exception as ee:
                    print ee
                    return HttpResponse("Sorry! Some error occured.")
            else:
                return HttpResponse("gstin number is already taken")


    def get(self,request):
        #import pdb;pdb.set_trace()
        _params = request.GET
        idlist = _params.get('idlist')
        if not idlist:
            return HttpResponse("Please enter organisation ID(s)")
        else:
            try:
                idlist = eval(idlist)
                out = Org.objects.filter(org_id__in = idlist)
                if not out:
                    return HttpResponse("The requested Organisation ids don't exist.")
                else:
                    res = [obj.as_dict() for obj in out]
                    #out = serializers.serialize('json', out)
                    return JsonResponse(res,safe=False)
            except TypeError:
                return HttpResponse("Please enter a valist LIST and not an integer.")
            except SyntaxError:
                return HttpResponse("Please check the syntax of the list")
            except Exception:
                return HttpResponse("Sorry! Some error occured")
    def put(self,request):
        _params = QueryDict(request.body)
        org_id = _params.get('org_id')
        if not org_id:
            return HttpResponse("Enter organisation id for updating details")
        else:
            address = _params.get('address')
            gstin_no = _params.get('gstin_no')
            basic_currency = _params.get('basic_currency')
            if not address or not gstin_no or not basic_currency:
                return HttpResponse("You cannot delete the required data")
            else:
                import pdb; pdb.set_trace()
                orgs = Org.objects.filter(gstin_no = gstin_no).exclude(org_id = org_id)

                if not orgs:
                    obj = Org.objects.filter(org_id = org_id)
                    if not obj:
                        return HttpResponse("Organisation not found")
                    else:
                        obj.update(address = address, gstin_no = gstin_no, basic_currency = basic_currency)
                        return HttpResponse("Organisation instance updated successfully for organisation with id " + org_id)
                else:
                    return HttpResponse("gstin number already exists")


class user(View):

    def post(self,request):
        #import pdb;pdb.set_trace()
        _params = request.POST
        id = _params.get('id')
        email = _params.get('email')
        phone_number = _params.get('phone_number')
        org_details = _params.get('org_details')
        if not id or not email or not phone_number or not org_details:
            return HttpResponse("Fill all fields")
        else:
            c = UserDetails.objects.filter(email = email)
            cd = '\n'.join(d.email for d in c)
            if not cd:
                try:
                    orga = Org.objects.get(org_id = org_details)
                    UserDetails.objects.create(id = id, email = email, phone_number = phone_number, org_details = orga)
                    return HttpResponse("user registered Successfully")
                except ObjectDoesNotExist as ex:
                    print ex
                    return HttpResponse("organisation with the produced details doesn't exist.")
                except IntegrityError:
                    return HttpResponse("Organisation with the following id already exists")
                except Exception as e:
                    print e
                    return HttpResponse("Sorry! Some error occured")
            else:
                return HttpResponse("The email is already taken")

    def get(self,request):
        #import pdb;pdb.set_trace()
        _params = request.GET
        idlist = _params.get('idlist')
        if not idlist:
            return HttpResponse("Please enter organisation ID(s)")
        else:
            try:
                idlist = eval(idlist)
                output = UserDetails.objects.filter(id__in = idlist).values()
                if not output:
                    return JsonResponse("The organisation details requested , doesn't exist.", safe = False)
                else:
                    output = list(output)
                    return JsonResponse(output, safe=False)
            except TypeError:
                return HttpResponse("Please enter the integer in form of a list")
            except SyntaxError:
                return HttpResponse("Please check the Syntax of list")
            except Exception:
                return HttpResponse("Sorry! Some error occured")

    def put(self,request):
        _params = QueryDict(request.body)
        id = _params.get('id')
        email = _params.get('email')
        phone_number = _params.get('phone_number')
        org_details = _params.get('org_details')
        if not id or not email or not phone_number or not org_details:
            return HttpResponse("You can't delete required fields. You can only update them")
        else:
            try:
                c = UserDetails.objects.filter(email = email).exclude(id = id)
                if not c:
                    orgs = Org.objects.get(org_id = org_details)
                    obj = UserDetails.objects.filter(id = id)
                    obj.update(email = email, phone_number = phone_number, org_details = orgs)
                    return HttpResponse("User instance updated successfully for organisation with id " + id)
                else:
                    return HttpResponse("Email already exists")
            except ObjectDoesNotExist:
                return HttpResponse("The organisation details you want to update doesn't exist.")
            except IntegrityError:
                return HttpResponse("User that you are trying to access doesn't exist. Instead register as a user.")
            except Exception as e:
                print e
                return HttpResponse("Sorry! some error occured")

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Flight , City , User, Bookings,Org,UserDetails


admin.site.register(Flight)
admin.site.register(City)
admin.site.register(User)
admin.site.register(Bookings)
admin.site.register(Org)
admin.site.register(UserDetails)

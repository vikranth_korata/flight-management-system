from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
#from . import views
from .views import *
#from views import FlightProject

urlpatterns = [

    url(r'^flights/(?P<fromcity>.*)/(?P<tocity>.*)$',flights.as_view(), name='flights'),
    url(r'^bookings/(?P<username>.*)$',bookings.as_view(), name = 'bookings'),
    url(r'^flightsbyairline/(?P<airline>.*)$',flightsbyairline.as_view() , name = 'flightsbyairline'),
    url(r'^booking/create/(?P<airline>.*)/(?P<username>.*)$',booking.as_view(), name = 'booking'),
    url(r'^findcode/(?P<cityname>.*)$', citycode.as_view(), name='city'),
    url(r'^register/(?P<name>.*)/(?P<age>.*)/(?P<passport>.*)/(?P<email>.*)$', register.as_view(), name = 'register'),
    url(r'^org/$',csrf_exempt(org.as_view()),name = 'org'),
    url(r'^user/$',csrf_exempt(user.as_view()), name = 'user')



]
